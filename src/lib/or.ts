import {Middleware} from "@arabesque/core";

/**
 * Create a "OR" middleware.
 *
 * https://en.wikipedia.org/wiki/OR_gate
 */
export const createORMiddleware = <Context>(middleware: Middleware<Context>, ...middlewares: Array<Middleware<Context>>): Middleware<Context> => {
    return (context, next) => {
        let output: boolean = false;

        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<Context>>): Promise<Context> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            return middleware(context, (context) => {
                output = true;

                return Promise.resolve(context);
            }).then((context) => {
                return output ? Promise.resolve(context) : invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, [middleware, ...middlewares]).then((context) => {
            return output ? next(context) : Promise.resolve(context);
        });
    };
}