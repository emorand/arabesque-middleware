import {Middleware} from "@arabesque/core";

/**
 * Create a "AND" middleware.
 *
 * https://en.wikipedia.org/wiki/AND_gate
 */
export const createANDMiddleware = <Context>(middleware: Middleware<Context>, ...middlewares: Array<Middleware<Context>>): Middleware<Context> => {
    return (context, next) => {
        let output: boolean = false;

        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<Context>>): Promise<Context> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            output = false;

            return middleware(context, (context) => {
                output = true;

                return invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, [middleware, ...middlewares]).then((context) => {
            return output ? next(context) : Promise.resolve(context);
        });
    };
}