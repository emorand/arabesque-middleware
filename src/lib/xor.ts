import {Middleware} from "@arabesque/core";

/**
 * Create a "XOR" middleware.
 *
 * https://en.wikipedia.org/wiki/XOR_gate
 */
export const createXORMiddleware = <Context>(middleware: Middleware<Context>, ...middlewares: Array<Middleware<Context>>): Middleware<Context> => {
    return (context, next) => {
        let state: number = 0;

        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<Context>>): Promise<Context> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            return middleware(context, (context) => {
                state++;

                return Promise.resolve(context);
            }).then((context) => {
                return state > 1 ? Promise.resolve(context) : invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, [middleware, ...middlewares]).then((context) => {
            return state === 1 ? next(context) : Promise.resolve(context);
        });
    };
}