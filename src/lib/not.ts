import {Middleware} from "@arabesque/core";

/**
 * Create a "NOT" middleware.
 *
 * https://en.wikipedia.org/wiki/NOT_gate
 */
export const createNOTMiddleware = <Context>(middleware: Middleware<Context>): Middleware<Context> => {
    return (context, next) => {
        let output: boolean = false;

        return middleware(context, (context) => {
            output = true;

            return Promise.resolve(context);
        }).then((context) => {
            return output ? Promise.resolve(context) : next(context);
        });
    };
}